from django.db import models

# Create your models here.
class Rgph(models.Model):
    nomRgph = models.CharField()
    annee = models.PositiveSmallIntegerField()
    rgphFile = models.FileField()

    def __str__(self) -> str:
        return self.nomRgph
    
    class Meta:
        ordering = "anne"

class Region(models.Model):
    nom = models.CharField()
    nombreMasculin = models.PositiveIntegerField()
    nombreFeminin = models.PositiveIntegerField()
    rgph = models.ForeignKey(Rgph, on_delete=models.CASCADE)
    createDate = models.DateTimeField()

    def __str__(self) -> str:
        return self.nom


class Prefecture(models.Model):
    nom = models.CharField()
    nombreMasculin = models.PositiveIntegerField()
    nombreFeminin = models.PositiveIntegerField()
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    createDate = models.DateTimeField()

    def __str__(self) -> str:
        return self.nom

class Commune(models.Model):
    nom = models.CharField()
    nombreMasculin = models.PositiveIntegerField()
    nombreFeminin = models.PositiveIntegerField()
    prefecture = models.ForeignKey(Prefecture, on_delete=models.CASCADE)
    createDate = models.DateTimeField()

    def __str__(self) -> str:
        return self.nom


class Canton(models.Model):
    nom = models.CharField()
    nombreMasculin = models.PositiveIntegerField()
    nombreFeminin = models.PositiveIntegerField()
    commune = models.ForeignKey(Commune, on_delete=models.CASCADE)
    createDate = models.DateTimeField()

    def __str__(self) -> str:
        return self.nom

class Ville(models.Model):
    nom = models.CharField()
    nombreMasculin = models.PositiveIntegerField()
    nombreFeminin = models.PositiveIntegerField()
    createDate = models.DateTimeField()

    def __str__(self) -> str:
        return self.nom