from django.apps import AppConfig


class RgphConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rgph'
